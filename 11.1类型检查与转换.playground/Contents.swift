import UIKit

//Swift语言中要判断某个实例是否属于某个具体类型，可以使用is关键字，使用is关键字组成的判断语句将返回一个布尔值，开发者可以根据布尔值的真或者假来判断类型检查结果是否匹配

var str = "字符串"
var num = 1

if str is String {
    print("str是String")
}else{
    print("str不是String")
}

if num is Int {
    print("num是Int")
}

//对于有继承关系的类，类型检查有如下原则：
//· 子类实例进行父类类型的检查可以检查成功。
//· 父类实例进行子类类型的检查不可以检查成功。

//基类
class BaseClass {
    
}

//子类
class SubClass:BaseClass{
    
}

var cls1 = BaseClass()
var cls2 = SubClass()

//使用父类实例进行子类类型检查,返回false
print("cls1是BaseClass吗:\(cls1 is SubClass)")

//使用子类实例进行父类类型检查,返回true
print("cls2是BaseClass吗:\(cls2 is BaseClass)")


//关于类型转换，Swift语言中使用的是as关键字。与类型检查相似，Swift语言中类型的转换有着向上兼容与向下转换的原则
//· 一个父类类型的集合可以接收子类类型的实例。
//· 在使用第1条原则中父类集合中的实例时，可以将其转换为子类类型。

//在使用类型转换时，需要使用as?或者as!方式。as?是一种比较安全的转换方式，其会将类型转换后的结果映射为Optional值，如果类型转换成功，则值为原实例，如果类型转换失败，则会返回Optional值nil。而as!是一种强制转换方式，其默认此次转换一定成功，如果转换失败，则会产生运行时错误，程序会崩溃。开发者在使用as!进行类型转换时，必须保证实例的真实类型和要转换的类型一致。
