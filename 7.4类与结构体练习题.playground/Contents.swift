import UIKit

var str = "Hello, playground"
//（1）设计一个学生类，为每个学生设计姓名、年龄和性别属性，为其提供一个学习的方法。
enum Gender {
    case 男
    case 女
}

class Student {
    let gender:Gender
    let name:String
    let age:Int
    init(gender:Gender,name:String,age:Int) {
        self.gender = gender
        self.name = name
        self.age = age
    }
    func study() {
        print("学生:\(name)学习知识")
    }
    
}

//测试
let xiaoming = Student(gender: .男, name: "小明", age: 12)
xiaoming.study()

//（2）结合第1题中的学生类设计一个班级类，其中需要有班级名、学生人数和班长3个属性，并设计转入学生与转出学生的方法。
class Class {
    var name:String
    var studentCount:Int
    var monitor:Student
    
    init(name:String,studentCount:Int,monitor:Student) {
        self.name = name
        self.studentCount = studentCount
        self.monitor = monitor
    }
    
    func addStudent() {
        studentCount += 1
    }
    func deleteStudnet() {
        if studentCount > 0 {
            studentCount -= 1
        }
    }
}

//创建一个学生为班长
let monitor = Student(gender: .女, name: "丽丽", age: 14)
//创建一个班级
let class1 = Class(name: "三年二班", studentCount: 34, monitor: monitor)
//转入一个学生
class1.addStudent()
print(class1.studentCount)

//（3）结合第1题中设计的学生类，设计一个老师类，为老师类提供一个教学科目、姓名和所教学生列表的属性，并为老师类提供一个教学方法，在教学方法中进行学生的学习行为。
enum Subject {
    case 语文
    case 数学
    case 英语
}

//老师类
class Teacher {
    let name:String
    let subject:Subject
    var studentArray:Array<Student>
    
    init(name:String,subject:Subject,studnets:Array<Student>){
        self.name = name
        self.subject = subject
        self.studentArray = studnets
    }
    
    func teach() {
        for item in studentArray {
            item.study()
        }
    }
}

//测试:
//创建3个学生
let xiaowang = Student(gender: .男, name: "小王", age: 12)
let xiaoli = Student(gender: .女, name: "小李", age: 14)
let laozhang = Student(gender: .男, name: "老张", age: 24)

//创建老师
let teacher = Teacher(name: "王阳", subject: .语文, studnets: [xiaowang,xiaoli,laozhang])
//进行教学
teacher.teach()
