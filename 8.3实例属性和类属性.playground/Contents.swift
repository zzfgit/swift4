import UIKit

var str = "Hello, playground"

//Swift语言中与之对应的还有类属性。
//实例属性由类的实例调用，类属性则是直接由类来调用。
//可以简单理解为，实例属性是与具体实例相关联的，
//一般用来描述类实例的一些特性，而类属性是与此类型相关联的，
//用来描述整个类型的某些特性。类属性使用static或者class关键字来声明
//。使用static关键字声明的属性也被称为静态属性，需要注意，
//对于类计算属性，如果允许子类对其计算方法进行覆写，则需要用class关键字来声明，

class SomeClass {
    //静态存储属性
    static var className = "SomeClass"
    //静态计算属性
    static var subName:String{
        return "Sub"+className
    }
    class var classSubName:String {
        return "Class" + subName
    }
}

//雷属性不需要创建实例对象,直接使用类名来调用
print(SomeClass.className)
print(SomeClass.subName)
print(SomeClass.classSubName)

//创建一个集成与SomeClass的子类
class SubClass:SomeClass{
    //对计算类属性计算方法进行覆写,需要使用override关键字
    override class var classSubName: String{
        return "NewName"
    }
}

print(SubClass.classSubName)


//类属性通常用来描述整个事物类所共享的一些事物特点，例如生产一批电视机，电视机的标准寿命值可以作为类属性。
