//
//  GameVC.swift
//  ChineseChess
//
//  Created by 左忠飞 on 2021/1/21.
//

import UIKit

class GameVC: UIViewController,GameEngineDelegate {
    
    //游戏结束
    func gameOver(redWin: Bool) {
        if redWin {
            tipLabel.text = "红方胜"
            tipLabel.textColor = .red
        }else{
            tipLabel.text = "绿方胜"
            tipLabel.textColor = .green
        }
        tipLabel.isHidden = false
        settingButton?.isEnabled = true
        settingButton?.backgroundColor = .green
    }
    
    //切换先行方
    func couldRedMove(red: Bool) {
        if red {
            settingButton?.setTitle("红方先行", for: .normal)
        }else{
            settingButton?.setTitle("绿方先行", for: .normal)
        }
    }
    

    var chessBoard:ChessBoard?
    
    var gameEngine:GameEngine?
    
    //开始游戏按钮
    var startGameButton:UIButton?
    //切换先手方按钮
    var settingButton:UIButton?
    //胜负提示
    var tipLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        chessBoard = ChessBoard(origin:CGPoint())
        chessBoard = ChessBoard(origin:CGPoint(x:20,y:80))
        self.view.addSubview(chessBoard!)
        
        //游戏引擎实例化
        gameEngine = GameEngine(board: chessBoard!)
        gameEngine?.delegate = self
        
        //开始游戏按钮
        startGameButton = UIButton(type: .system)
        startGameButton?.frame = CGRect(x: 40, y: self.view.frame.size.height-120, width: self.view.frame.size.width/2-80, height: 30)
        startGameButton?.backgroundColor = .orange
        startGameButton?.setTitle("开始游戏", for: .normal)
        self.view.addSubview(startGameButton!)
        startGameButton?.setTitleColor(.white, for: .normal)
        startGameButton?.layer.cornerRadius = 5
        startGameButton?.clipsToBounds = true
        startGameButton?.addTarget(self, action: #selector(startGame), for: .touchUpInside)
        
        //设置先行方按钮按钮
        settingButton = UIButton(type: .system)
        settingButton?.frame = CGRect(x: self.view.frame.size.width/2+40, y: self.view.frame.size.height-120, width: self.view.frame.size.width/2-80, height: 30)
        settingButton?.backgroundColor = .orange
        settingButton?.setTitle("红方先行", for: .normal)
        settingButton?.setTitleColor(.white, for: .normal)
        settingButton?.layer.cornerRadius = 5
        settingButton?.clipsToBounds = true
        self.view.addSubview(settingButton!)
        settingButton?.addTarget(self, action: #selector(settingGame), for: .touchUpInside)
        
        
        //提示label
        tipLabel.frame = CGRect(x: self.view.frame.size.width/2 - 100, y: 200, width: 200, height: 60)
        tipLabel.backgroundColor = .white
        tipLabel.layer.cornerRadius = 15
        tipLabel.clipsToBounds = true
        tipLabel.font = UIFont.systemFont(ofSize: 25)
        tipLabel.textColor = .red
        tipLabel.textAlignment = .center
        tipLabel.isHidden = true
        self.view.addSubview(tipLabel)
    }

    @objc func startGame(btn:UIButton){
        tipLabel.isHidden = true
        gameEngine?.startGame()
        settingButton?.backgroundColor = .gray
        settingButton?.isEnabled = false
        btn.setTitle("重新开局", for:.normal)
    }
    @objc func settingGame(btn:UIButton){
        if btn.title(for: .normal) == "红方先行" {
            gameEngine?.setRedFirstMove(red: false)
            btn.setTitle("绿方先行", for: .normal)
        }else{
            gameEngine?.setRedFirstMove(red: true)
            btn.setTitle("红方先行", for: .normal)
        }
    }
}
