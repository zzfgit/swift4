//
//  ChessItem.swift
//  ChineseChess
//
//  Created by 左忠飞 on 2021/1/21.
//

import UIKit

class ChessItem: UIButton {

    //这个属性标记棋子的选中状态
    var selectedState:Bool = false
    //这个属性标记是否为红方棋子
    var isRed = true
    //提供一个自定义的构造方法
    init(center:CGPoint) {
        //根据屏幕尺寸决定棋子大小
        let screenSize = UIScreen.main.bounds.size
        let itemSize = CGSize(width: (screenSize.width - 40)/9-4,
                              height: (screenSize.width - 40)/9-4)
        
        super.init(frame: CGRect(origin: CGPoint(x: center.x - itemSize.width/2, y: center.y-itemSize.width/2), size: itemSize))
        installUI()
    }
    
    //棋子UI的设计
    func installUI(){
        self.backgroundColor = .white
        self.layer.masksToBounds = true
        self.layer.cornerRadius = ((UIScreen.main.bounds.size.width-40)/9-4)/2
        self.layer.borderWidth = 0.5
    }
    
    //设置棋子标题,isOwn属性决定是己方还是敌方
    func setTitle(title:String,isOwn:Bool) {
        self.setTitle(title, for: .normal)
        if isOwn {
            self.layer.borderColor = UIColor.red.cgColor
            self.setTitleColor(.red, for: .normal)
            self.isRed = true
        }else{
            self.layer.borderColor = UIColor.green.cgColor
            self.setTitleColor(.green, for: .normal)
            self.isRed = false
            
            //敌方的棋子要旋转180度
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }
    
    //将棋子设置为选中状态
    func setSelectedStated(){
        if !selectedState {
            selectedState = true
            self.backgroundColor = .purple
        }
    }
    
    //将棋子设置为非选中状态
    func setUnselectedState(){
        if selectedState {
            selectedState = false
            self.backgroundColor = .white
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
