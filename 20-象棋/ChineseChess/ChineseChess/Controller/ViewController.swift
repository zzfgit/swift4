//
//  ViewController.swift
//  ChineseChess
//
//  Created by 左忠飞 on 2021/1/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var startGameBtn: UIButton!
    @IBOutlet weak var musicBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //读取用户音频设置状态
        if UserInfoManager.getAudioState() {
            musicBtn.setTitle("音乐：开", for: .normal)
            MusicEngine.shareInstance.playBackgroundMusic()
        }else{
            musicBtn.setTitle("音乐：关", for: .normal)
            MusicEngine.shareInstance.stopBackgroundMusic()
        }
    }

    @IBAction func startGameBtnClick(_ sender: Any) {
        let gameVC = GameVC()
        self.present(gameVC,animated:true,completion:nil)
    }
    
    //点击音频设置按钮后,进行状态的切换
    @IBAction func musicBtnClick(_ sender: Any) {
        if UserInfoManager.getAudioState() {
            musicBtn.setTitle("音乐：关", for: .normal)
            UserInfoManager.setAudioState(isOn: false)
            MusicEngine.shareInstance.stopBackgroundMusic()
        }else{
            musicBtn.setTitle("音乐：开", for: .normal)
            UserInfoManager.setAudioState(isOn: true)
            MusicEngine.shareInstance.playBackgroundMusic()
        }
    }
}

