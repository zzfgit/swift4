//
//  TipButton.swift
//  ChineseChess
//
//  Created by 左忠飞 on 2021/1/22.
//

import UIKit

//该类用于标记当前选中棋子可以移动到的位置
class TipButton: UIButton {

    //提供一个自定义的构造方法
    init(center:CGPoint) {
        super.init(frame: CGRect(x: center.x - 20, y: center.y - 20, width: 40, height: 40))
        installUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installUI() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 20
        self.backgroundColor = .orange
        self.alpha = 0.3
    }
    
}
