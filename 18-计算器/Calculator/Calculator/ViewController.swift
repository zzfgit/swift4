//
//  ViewController.swift
//  Calculator
//
//  Created by 左忠飞 on 2021/1/16.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController: UIViewController, BoardButtonInputDelegate {
    func boardButtonClick(content: String) {
        if (content == "AC" ||
            content == "Delete" ||
            content == "="){
            
            switch content {
            case "AC":
                screen.clearContent()
                screen.refreshHistory()
            case "Delete":
                screen.deleteInput()
            case "=":
                let result = calculator.calculatEquation(equation: screen.inputString)
                //刷新历史
                screen.refreshHistory()
                //清除输入的内容
                screen.clearContent()
                //将结果输入
                screen.inputContent(content: String(result))
                isNew = true
            default:
                screen.refreshHistory()
            }
            
        }else{
            screen.inputContent(content: content)
        }
    }
    
    
    let board = Board()
    let screen = Screen()
    //计算引擎实例
    let calculator = CalculatorEngine()
    //这个输入是否需要刷新显示屏
    var isNew = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.installUI()
    }

    func installUI() {
        self.view.addSubview(board)
        board.delegate = self
        board.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.top.equalTo(200)
        }
        
        
        self.view.addSubview(self.screen)
        self.screen.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(0)
            make.bottom.equalTo(self.board.snp.top)
        }
    }
    
}

