//
//  Screen.swift
//  Calculator
//
//  Created by 左忠飞 on 2021/1/16.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class Screen: UIView {

    //显示用户输入信息
    var inputLabel:UILabel?
    //显示历史记录信息
    var historyLabel:UILabel?
    //用户输入表达式或者计算结果字符串
    var inputString = ""
    //历史记录表达式字符串
    var historyString = ""
    //所有数字字符,用于进行检测匹配
    let figureArray:Array<Character> = ["0","1","2","3","4","5","6","7","8","9","."]
    //所有运算符字符,用于进行检测匹配
    let funcArray = ["+","-","*","/","%","^"]
    //默认构造方法
    init() {
        inputLabel = UILabel()
        historyLabel = UILabel()
        super.init(frame:CGRect.zero)
        installUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installUI() {
        inputLabel?.textAlignment = .right
        historyLabel?.textAlignment = .right
        
        inputLabel?.font = UIFont.systemFont(ofSize: 34)
        historyLabel?.font = UIFont.systemFont(ofSize: 30)
        
        inputLabel?.textColor = .orange
        historyLabel?.textColor = .black
        
        //文字大小根据字数进行适配
        inputLabel?.adjustsFontSizeToFitWidth = true
        inputLabel?.minimumScaleFactor = 0.5
        historyLabel?.adjustsFontSizeToFitWidth = true
        historyLabel?.minimumScaleFactor = 0.5
        
        //设置文字截断方式
        inputLabel?.lineBreakMode = .byTruncatingHead
        historyLabel?.lineBreakMode = .byTruncatingHead
        
        inputLabel?.numberOfLines = 0
        historyLabel?.numberOfLines = 0
        
        self.addSubview(inputLabel!)
        self.addSubview(historyLabel!)
        
        //进行布局约束
        inputLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.bottom.equalTo(-10)
            make.height.equalTo(inputLabel!.superview!.snp_height).multipliedBy(0.5).offset(-10)
        })
        
        historyLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(10)
            make.height.equalTo(inputLabel!.superview!.snp_height).multipliedBy(0.5).offset(-10)
        })
    }
    
    //提供一个信息输入的接口
    func inputContent(content:String){
        
        //如果不是数字也不是运算符就不处理
//        if figureArray.contains(content.c) {
//            <#code#>
//        }
        
        //        if inputString.count > 0 {
        //
        //        }
        
        //如果不是数字也不是运算符就不处理
        if (!figureArray.contains(content.last!) &&
            !funcArray.contains(content)){
            return
        }
        
        //如果不是首次输入字符
        if !inputString.isEmpty {
            //数字后边可以任意输入
            if figureArray.contains(inputString.last!) {
                inputString.append(content)
                inputLabel?.text = inputString
            }else{
                //运算符后边只能输入数字
                if figureArray.contains(content.last!) {
                    inputString.append(content)
                    inputLabel?.text = inputString
                }
            }
        }else{
            //只有数字可以作为首个字符
            if figureArray.contains(content.last!) {
                inputString.append(content)
                inputLabel?.text = inputString
            }
        }
    }
    
    //提供一个刷新历史记录的方法
    func refreshHistory() {
        historyString = inputString
        historyLabel?.text = historyString
    }

    //清空显示屏中当前输入的信息
    func clearContent() {
        inputString = ""
        inputLabel?.text = inputString
    }
    
    //删除显示屏中上次输入的字符
    func deleteInput(){
        if !inputString.isEmpty {
            inputString.removeLast()
            inputLabel?.text = inputString
        }
    }
}
