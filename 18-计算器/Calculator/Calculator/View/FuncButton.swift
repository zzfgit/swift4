//
//  FuncButton.swift
//  Calculator
//
//  Created by 左忠飞 on 2021/1/16.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class FuncButton: UIButton {

    init() {
        super.init(frame: CGRect.zero)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 219/255.0, green: 219/255.0, blue: 219/255.0, alpha: 1).cgColor
        
        self.setTitleColor(.orange, for: .normal)
        self.setTitleColor(.black, for: .highlighted)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 25)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
