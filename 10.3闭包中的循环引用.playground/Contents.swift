import UIKit

var str = "Hello, playground"

class Person {
    var name:String = "姓名"
    //闭包中引用了self,造成了循环引用无法释放
    lazy var closure:() -> Void = {
        print(self.name)
    }
    deinit {
        print("Person类销毁了")
    }
}

var xiaowang:Person? = Person()
xiaowang?.closure
xiaowang = nil

class Person2 {
    var name:String = "姓名"
    
    lazy var closure:() -> Void = {
        //使用捕获列表对闭包中使用到的self进行无主引用的转换
        [unowned self]() -> Void in
        print(self.name)
    }
    deinit {
        print("Person2类销毁了")
    }
}


var xiaoming:Person2? = Person2()
xiaoming?.closure
xiaoming = nil
