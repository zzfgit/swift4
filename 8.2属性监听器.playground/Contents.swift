import UIKit

var str = "Hello, playground"

//属性监听器,可以在存储属性赋值前和赋值后进行监听,并写一些代码
//只有存储属性可以进行属性监听,计算型属性不行
//在对象的构造方法中给属性赋值不会触发属性监听器,之后再次修改属性值才会触发属性监听器
class Teacher {
    var name:String {
        willSet {
            print("将要设置name的值为:\(newValue)")
        }
        didSet {
            print("name值已经被修改,旧的值为:\(oldValue)")
        }
    }
    
    //在监听其中给新值和旧值自定义变量名
    var age:Int {
        willSet(newAge){
            print("将要设置新的age值:\(newAge)")
        }
        didSet(oldAge){
            print("age的值已经被修改,旧的值为:\(oldAge)")
        }
    }
    
    init(name:String,age:Int) {
        self.name = name
        self.age = age
    }
}

var wanglaoshi = Teacher(name: "王老师",age: 30)

wanglaoshi.name = "王刚"
wanglaoshi.age = 28

