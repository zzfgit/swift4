import UIKit

var str = "Hello, playground"

//对于类的构造方法，实际开发中也经常有使用到构造失败的情况。举个例子，一个构造方法可能需要一些特定情况的参数，当传递的参数不符合要求时，开发者需要让这次构造失败，这时就需要使用到Swift语言中可失败构造方法的相关语法。可失败构造方法的定义十分简单，只需要使用init?()即可，在实现可失败构造方法时，开发者可以根据需求返回nil

class Person {
    var name:String
    init(name:String) {
        self.name = name
    }
    
    init?(param:Bool){
        guard param else {
            return nil
        }
        name = "默认名"
    }
}

var xiaowang = Person(param: false)
var liming = Person(name: "黎明")


//开发者也可以设置某些构造方法为必要构造方法，如果一个类中的某些构造方法被指定为必要构造方法，则其子类必须实现这个构造方法（可以通过继承或者覆写的方式），必要构造方法需要使用required关键字进行修饰

class Person2 {
    var name:String
    //此方法必须被子类实现
    required init (name:String){
        self.name = name
    }
    init?(param:Bool){
        //使用守护语句,当param为true时才进行构造
        guard param else {
            return nil
        }
        name = "默认名"
    }
}

class Teacher2:Person2 {
    required init(name: String) {
        super.init(name: name)
    }
}


//关于属性的构造，还有一个小技巧，如果某些属性的构造比较复杂，开发者可以通过闭包的方式来进行属性的构造，示例代码如下：
class Person3 {
    var name:String
    required init(name:String){
        self.name = name
    }
    
    var age:Int = {
        return 9 + 9
    }()
}
//由于闭包是一个代码块，因此使用闭包的方式可以将多行功能代码组合起来使用，这对于复杂类型属性的构造十分方便。注意，闭包后面的小括号()不可漏掉，失去了小括号，这个属性就会变成一个只读的计算属性，本质就发生了变化。



//析构方法
//析构方法与构造方法是互逆的，如果将构造方法理解为类实例的创建过程，则析构方法就是类实例的销毁过程。在实际开发中，经常需要在类实例将要销毁的时候将其中用到的资源释放掉，如关闭文件等操作都会放入析构方法中进行。构造方法使用init()来标识，析构方法使用deinit()来标识
class Temp {
    deinit {
        print("Temp实例被销毁")
    }
}
var temp:Temp? = Temp()
//当可选类型的实例变量被赋值为nil时,就会被释放
temp = nil
