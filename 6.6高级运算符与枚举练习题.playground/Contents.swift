import UIKit

var str = "Hello, playground"
//（1）模拟C语言通过自定义运算符的方式实现前缀自增、前缀自减、后缀自增、后缀自减运算符。
//定义前缀自增运算符
prefix operator ++
//定义后缀自增运算符
postfix operator ++
//定义前缀自减运算符
prefix operator --
//定义后缀自减运算符
postfix operator --

//实现运算符
prefix func ++ (para: inout Int) -> Int{
    para += 1
    return para
}

postfix func ++ (para: inout Int) -> Int {
    para += 1
    return para - 1
}

prefix func -- (para: inout Int) -> Int {
    para -= 1
    return para
}

postfix func -- (para: inout Int) -> Int {
    para -= 1
    return para + 1
}

//测试
var num1:Int = 5
print(num1++)//5

var num2:Int = 5
print(++num2)//6

var num3:Int = 5
print(num3--)//5

var num4:Int = 5
print(--num4)//4


//（2）Swift语言中的加法运算符不能支持对区间范围的相加操作，重载加法运算符，使其支持区间的追加，例如(0…5)+5计算后的结果为区间0…10。
func + (param:ClosedRange<Int>,param2:Int) -> ClosedRange<Int> {
    return param.lowerBound...param.upperBound + param2
}
//测试
var newRange = 0...5+5
print(newRange)

//（3）自定义新后缀运算符“*!”，其功能是对某个数进行阶乘计算。
//定义后缀运算符
postfix operator *!
//实现 *! 运算符
postfix func *! (param:Int) -> Int {
    var result = 1
    var tmp = param
    while tmp > 0 {
        result *= tmp
        tmp - 1
    }
    return result
}

//测试不通过,原因未知


//（4）模拟设计一个交通工具枚举，将速度与乘坐价钱作为枚举的相关值。
enum Transport {
    case car(price:Int,speed:Float)
    case boat(price:Int,speed:Float)
    case airport(price:Int,speed:Float)
}

//创建一个汽车交通工具,价钱为2,速度为10
var car = Transport.car(price: 2, speed: 10)


