import UIKit

var str = "Hello, playground"

//指定构造方法不需要任何关键字修饰，便利构造方法需要使用Convenience关键字来修饰。指定构造方法是类的基础构造方法，任何类都要至少有一个指定构造方法；便利构造方法则是为了方便开发者使用，为类额外添加构造方法。需要注意，便利构造方法最终也要调用指定构造方法。

//· 子类的指定构造方法中必须调用父类的指定构造方法。
//· 便利构造方法中必须调用当前类的其他构造方法。
//· 便利构造方法归根结底要调用到某个指定构造方法。

class SuperClass {
    init() {
        print("基类的指定构造方法")
    }
    convenience init(name:String) {
        print("基类的便利构造方法")
        self.init()
    }
}

class SubClass:SuperClass {
    
    //覆写父类的指定构造方法必须调用父类的指定构造方法
    override init() {
        super.init()
    }
    
    //便利构造方法必须调用自己的其他构造方法
    convenience init(para:String){
        self.init()
    }
    
    //构造方法调用另一个便利构造方法
    convenience init(para:Int) {
        self.init(para:"name")
    }
    
    //注意:便利方法的调用联调最后必须调用某一个指定的构造方法
}
