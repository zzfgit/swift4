//
//  ViewController.swift
//  BaseUI
//
//  Created by 左忠飞 on 2021/1/6.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.label()
        self.button()
        self.imageView()
        self.textField()
        self.uiSwitch()
        self.segmentedControl()
        self.stepper()
        self.searchBar()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func label(){
        let label = UILabel(frame: CGRect(x: 20, y: 80, width: 200, height: 30));
        label.text = "我是一个普通的便签控件"
        self.view .addSubview(label)
        
        let label2 = UILabel(frame: CGRect(x: 20, y: 120, width: 200, height: 20));
        label2.text = "我是一个自定义的便签控件"
        label2.font = UIFont.boldSystemFont(ofSize: 20)
        label2.textColor = UIColor.red
        label2.shadowColor = UIColor.green
        label2.shadowOffset = CGSize(width: 2, height: 2)
        label2.textAlignment = .center
        view.addSubview(label2)

        let label3 = UILabel(frame: CGRect(x: 20, y: 140, width: 200, height: 100))
        label3.text = """
        第一行
        第二行
        第三行
        """
        label3.numberOfLines = 0;
        view .addSubview(label3)
    }
    
    func button(){
        let button1 = UIButton(type: .system)
        button1.frame = CGRect(x: 20, y: 250, width: 100, height: 30)
        button1.backgroundColor = UIColor.purple
        button1.setTitle("按钮标题", for: .normal)
        button1.setTitleColor(.white, for: .normal)
        button1.addTarget(self, action: #selector(button1Action), for: .touchUpInside)
        view.addSubview(button1)
        
    }
    @objc func button1Action() {
        print("点击了按钮")
    }
    
    func imageView(){
        //1:通过图片素材名称创建UIImage实例
        let image = UIImage(named: "红心")
        
        //2:公国文件路径创建UIImage实例
        let image2 = UIImage(contentsOfFile: "")
        
        //通过Data数据创建UIImage实例
        let image3 = UIImage(data: Data())
        
        let size = image?.size
        print("size:width\(size?.width),size:height\(size?.height)")
        
        let imageView = UIImageView(image:image)
        imageView.frame = CGRect(x: 20, y: 300, width: 100, height: 100)
        view.addSubview(imageView)
    }
    
    func textField() {
        let textField = UITextField(frame: CGRect(x: 20, y: 400, width: 300, height: 30))
        textField.borderStyle = .roundedRect
        textField.textColor = UIColor.red
        textField.textAlignment = .center
        textField.placeholder = "请输入姓名"
        view.addSubview(textField)
        
        //为输入框创建左视图
        let leftView = UIImageView(image: UIImage(named: "打分黄星星"))
        let rightView = UIImageView(image: UIImage(named: "打分黄星星"))
        textField.leftView = leftView
        textField.rightView = rightView
        
        textField.leftViewMode = .always
        textField.rightViewMode = .always
        textField.delegate = self
        textField.clearButtonMode = .always
    }
    
    func uiSwitch(){
        let swi = UISwitch()
        swi.center = CGPoint(x: 350, y: 350)
        swi.onTintColor = UIColor.green
        swi.tintColor = UIColor.red
        swi.thumbTintColor = .orange
        swi.setOn(true, animated: true)
        view.addSubview(swi)
        
        swi.addTarget(self, action:#selector(change), for: .valueChanged)
    }
    @objc func change(swi:UISwitch) {
        print("开关状态:\(swi.isOn)")
    }
    
    func segmentedControl() {
        //UISegmentedControl控件在UI展现上为一组切换按钮，常用于多界面切换的场景中，用户选中UISegmentedControl控件中一个分部按钮后完成界面的切换。
        let seg = UISegmentedControl(items: ["按钮1","按钮2asdf","按钮3f","按钮4asdfasdfasdf"])
        seg.frame = CGRect(x: 10, y: 450, width: 400, height: 30)
        seg.tintColor = .blue
        seg.backgroundColor = .white
        seg.selectedSegmentTintColor = .orange
        seg.addTarget(self, action: #selector(segAction), for: .valueChanged)
        //每个条目根据文字自动调整宽度
        seg.apportionsSegmentWidthsByContent = true
        view.addSubview(seg)
        
    }
    @objc func segAction(seg:UISegmentedControl) {
        print("选择了\(seg.selectedSegmentIndex)")
        if seg.selectedSegmentIndex == 1 {
            seg.insertSegment(withTitle: "新的", at: seg.numberOfSegments, animated: true)
        }
        if seg.selectedSegmentIndex == 2 {
            seg.removeSegment(at: seg.numberOfSegments - 1, animated: true)
        }
    }
    
    func stepper() {
        let stepper = UIStepper(frame: CGRect(x: 100, y: 500, width: 0, height: 0))
        stepper.tintColor = .red
        stepper.minimumValue = 0
        stepper.maximumValue = 10
        stepper.stepValue = 1
        stepper.addTarget(self, action: #selector(stepperAction), for: .valueChanged)
        //如果是true,会一直发送值变化事件,如果是false,当抬起手时才发送值发生变化事件
//        stepper.isContinuous = false
        
        //如果是true,按住不动会连续触发改变值,如果是false,按下一次只会触发一次值发生改变,长按不会连续改变值
        stepper.autorepeat = false
        
        //如果为true,值达到边界时会自动循环,比如到最小值,在减小就会变成最大值,如果是最大值,继续增大就会变成最小值,来回循环,
        //如果为false,达到边界值之后,再继续操作值就不会发生变化
        stepper.wraps = true;
        view.addSubview(stepper)
    }
    @objc func stepperAction(stepper:UIStepper) {
        print(stepper.value)
    }
    
    func searchBar(){
        let searchBar = UISearchBar(frame: CGRect(x: 20, y: 550, width: 300, height: 40))
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "搜索内容"
        //搜索控件的抬头标题
//        searchBar.prompt = "搜索控件"
        
        searchBar.showsCancelButton = true
        
        view.addSubview(searchBar)
    }
}

//写一个扩展来实现文本输入框的代理方法
extension ViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("改变的范围:\(range),内容:\(string)")
        return true
    }
}
