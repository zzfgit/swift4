import UIKit

var str = "Hello, playground"

//定义一个汽车的结构体
struct Car {
    //设置两个属性
    //价格
    var price:Int
    //品牌
    var brand:String
    //油量
    var petrol:Int
    
    //提供一个行路的方法
    mutating func drive() {
        if petrol > 0 {
            petrol -= 5
            print("行驶了10公里,油量还有\(self.petrol)")
        }
    }
    
    //提供一个加油的方法
    mutating func addPetrol() {
        petrol += 10
        print("加了10升油")
    }
}

//创建一个汽车结构体实例,价格为10000,品牌为奔驰,初始油量为10
var car = Car(price: 10000, brand: "奔驰", petrol: 10)
//使用.语法获取结构体实例的属性
print(car.price,car.brand,car.petrol)

//模拟汽车的行为
for _ in 0...6 {
    //如果油量为0就加油,否则就行驶
    if car.petrol == 0 {
        car.addPetrol()
    }else{
        car.drive()
    }
}

//结构体是值类型的,在进行数据传递时,值类型会复制一份,修改传递过来的数据,不会改变原来的结构体实例
//创建一个汽车结构体,价格为100000,品牌为宝马,油量初始值为10
var baoma = Car(price: 100000, brand: "宝马", petrol: 10)
print("这辆车是:\(baoma.brand),价格是:\(baoma.price),油量有:\(baoma.petrol)")

//创建另一个变量进行值的传递
var baoma2 = baoma
//修改baoma2的价格
baoma2.price = 500000

//打印baoma和baoma2
print("baoma:\(baoma)")//baoma:Car(price: 100000, brand: "宝马", petrol: 10)
print("baoma2:\(baoma2)")//baoma2:Car(price: 500000, brand: "宝马", petrol: 10)


//创建一个汽车类
class CarClass {
    //设置属性
    var price:Int
    var brand:String
    var petrol:Int
    
    func drive() {
        if petrol > 0 {
            petrol -= 1
            print("行驶了10公里")
        }
    }
    
    func addPetrol() {
        petrol += 10
        print("加了10升油")
    }
    
    init(price:Int,brand:String,petrol:Int) {
        self.price = price
        self.brand = brand
        self.petrol = petrol
    }
}

//类与结构体创建属性与方法的代码基本一致,不同点是:在结构体中不需要提供构造方法,结构体会自动根据属性生成一个构造方法
//但是在类中,必须提供构造方法,在构造方法中,完成对类的属性的赋值

//创建类的实例,访问类实例属性的方法:
var aodi = CarClass(price: 20000, brand: "奥迪", petrol: 3)
print("这辆车是:\(aodi.brand),价格是:\(aodi.price),油量有:\(aodi.petrol)")

for _ in 0...5 {
    if aodi.petrol > 0 {
        aodi.drive()
    }else{
        aodi.addPetrol()
    }
}

//类是引用类型,对类实例进行数据传递是不会复制,而是传递了实例的引用,
//如果将实例传递给新的变量,改变了新的变量的属性值,原来的实例的变量也会改变
var tesla = CarClass(price: 12000, brand: "特斯拉", petrol: 5)

var tesla2 = tesla
tesla2.price = 19000

print(tesla.price)
print(tesla2.price)
