import UIKit

var str = "Hello, playground"

//设计游戏中的子弹和敌人类，假设这是一款一维直线上的射击游戏，敌人的移动速度是10单位／帧，子弹的飞行速度是30单位／帧，当子弹碰到敌人后，子弹和敌人同时销毁。
//子弹类
class Bullet {
    //子弹的位置
    var place:Int
    
    //子弹的飞行速度
    static var speed:Int = 30
    
    //飞行方法
    func fly() {
        self.place += Bullet.speed
        print("子弹飞到了\(place)")
    }
    
    init(place:Int) {
        self.place = place
    }
    
    deinit {
        print("子弹命中")
    }
}


//敌人类
class Enemy {
    //位置
    var place:Int
    
    //敌人逃跑速度
    static var speed:Int = 10
    
    //逃跑行为
    func escape(){
        self.place += Enemy.speed
        print("敌人跑到了\(place)")

    }
    
    init(place:Int) {
        self.place = place
    }
    
    deinit {
        print("敌人被击中")
    }
}

var bullet:Bullet? = Bullet(place: 0)
var enemy:Enemy? = Enemy(place: 30)

//记录追击回合
var i = 0

while bullet!.place < enemy!.place {
    bullet!.fly()
    enemy!.escape()
    i+=1
    print("追击\(i)回合")
}

print("子弹击中敌人")

//敌人和子弹一起销毁
bullet = nil
enemy = nil
