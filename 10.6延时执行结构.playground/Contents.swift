import UIKit

//自定义的异常类型
enum MyError:Error {
    //定义3种程度的异常
    case DesTroyError
    case NormalError
    case SimpleError
}


var str = "Hello, playground"
//前面介绍过，延时构造语句lazy的作用是降低复杂类实例构造时所消耗的时间。Swift语言中还提供了一种延时执行结构，在函数中使用延时执行结构可以保证结构中的代码块始终在函数要结束时执行。通常情况下，延时执行结构会被用来释放函数中所使用的一些资源和关闭文件操作等。
//读者可能会疑惑，如果想让一部分代码在函数结束前才执行，那么直接将它放在函数的最后不就行了么？实际情况并非如此。在函数中，经常会由于特殊情况提前被break中断或者return返回；再或者，一个复杂的函数很可能会因为抛出异常而被中断。这些情况都会造成函数的结束，使用延时执行语句可以保证无论函数因为何种原因结束，在结束前都会执行延时结构块中的代码

func LazyExecute() throws -> Void {
    defer {
        print("finish")
    }
    
    print("执行函数")
    throw MyError.DesTroyError
}

try LazyExecute()
