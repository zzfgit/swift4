//
//  NoteLiistTVCTableViewController.swift
//  Notepad
//
//  Created by 左忠飞 on 2021/1/20.
//

import UIKit

class NoteLiistTVC: UITableViewController {

    //数据源
    var dataArray = Array<NoteModel>()
    //当前分组
    var name:String?
    
    //数据更新闭包
    var dataUpdated:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //设置导航栏标题
        self.title = name
        
        dataArray = DataManager.getNote(group: name!)
        
        //进行导航按钮的加载
        self.installNavigationItem()
    }
    
    func installNavigationItem() {
        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNote))
        let deleteItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteGroup))
        self.navigationItem.rightBarButtonItems = [addItem,deleteItem]
    }
    
    @objc func addNote() {
        let infoVC = NoteInfoVC()
        infoVC.group = name
        infoVC.isNew = true
        infoVC.dataUpdated = {()->Void in
            self.dataArray = DataManager.getNote(group: self.name!)
            self.tableView.reloadData()
        }

        self.navigationController?.pushViewController(infoVC, animated: true)
    }
    @objc func deleteGroup() {
        let alertController = UIAlertController(title: "警告", message: "您确定删除此分组下的所有记事吗?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "确定", style: .destructive) { (UIAlertAction) in
            DataManager.deleteGroup(name: self.name!)
            self.navigationController?.popViewController(animated: true)
            guard let update = self.dataUpdated else { return }
            update()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "noteListCellID"

        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: cellId)
        }
        let model = dataArray[indexPath.row]
        cell?.textLabel?.text = model.title
        cell?.detailTextLabel?.text = model.time
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let infoVC = NoteInfoVC()
        infoVC.group = name!
        infoVC.isNew = false
        infoVC.noteModel = dataArray[indexPath.row]
        infoVC.dataUpdated = {()->Void in
            self.dataArray = DataManager.getNote(group: self.name!)
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(infoVC, animated: true)
    }
}
