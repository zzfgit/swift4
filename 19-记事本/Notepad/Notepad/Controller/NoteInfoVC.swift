//
//  NoteInfoVC.swift
//  Notepad
//
//  Created by 左忠飞 on 2021/1/21.
//

import UIKit
import SnapKit

class NoteInfoVC: UIViewController {

    //当前编辑的记事数据模型
    var noteModel:NoteModel?
    //标题文本框
    var titleTextField:UITextField?
    //记事内容文本视图
    var bodyTextView:UITextView?
    //记事所属分组
    var group:String?
    var isNew = false
    
    //新增记事成功闭包
    var dataUpdated:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //消除导航对布局的影响
        self.edgesForExtendedLayout = UIRectEdge()
        self.view.backgroundColor = .white
        self.title = "记事"
        
        //进行界面的加载
        installUI()
        //进行导航功能按钮的加载
        installNavigationItem()
    }
    
    func installNavigationItem() {
        //创建两个导航功能按钮,用于保存与删除记事
        let saveItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(addNote))
        let deleteItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteNote))
        self.navigationItem.rightBarButtonItems = [saveItem,deleteItem]
    }

    @objc func keyBoardBeShow(notification:Notification) {
        
    }
    @objc func keyBoardBeHidden(notification:Notification) {
        
    }
    
    //添加记事
    @objc func addNote(){
        //如果是新记事本,进行数据库数据的新增
        if isNew {
            if titleTextField?.text != nil && (titleTextField?.text!.count)! > 0 {
                noteModel = NoteModel()
                noteModel?.title = titleTextField?.text!
                noteModel?.body = bodyTextView?.text
                
                //将当前时间进行格式化
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                noteModel?.time = dateFormatter.string(from: Date())
                noteModel?.group = group
                DataManager.addNote(note: noteModel!)
                self.navigationController?.popViewController(animated: true)
                guard let success = dataUpdated else { return }
                success()
            }
        }else{
            //更新记事
            if titleTextField?.text != nil && (titleTextField?.text!.count)! > 0 {
                noteModel?.title = titleTextField?.text!
                noteModel?.body = bodyTextView?.text
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                noteModel?.time = dateFormatter.string(from: Date())
                DataManager.updateNote(note: noteModel!)
                self.navigationController?.popViewController(animated: true)
                guard let success = dataUpdated else { return }
                success()
            }
        }
    }
    
    //删除记事
    @objc func deleteNote(){
        let alertController = UIAlertController(title: "警告", message: "您确定删除此条记事吗?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "确定", style: .destructive) { (UIAlertAction) in
            //如果不是新建记事,进行删除操作
            if !self.isNew {
                DataManager.deleteNote(note: self.noteModel!)
                self.navigationController?.popViewController(animated: true)
                guard let success = self.dataUpdated else { return }
                success()
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func installUI(){
        titleTextField = UITextField()
        self.view.addSubview(titleTextField!)
        titleTextField?.borderStyle = .none
        titleTextField?.placeholder = "请输入记事标题"
        titleTextField?.snp.makeConstraints({ (make) in
            make.top.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(30)
        })
        
        let line = UIView()
        self.view.addSubview(line)
        line.backgroundColor = .gray
        line.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(titleTextField?.snp_bottom as! ConstraintRelatableTarget).offset(5)
            make.right.equalTo(-15)
            make.height.equalTo(0.5)
        }
        
        bodyTextView = UITextView()
        bodyTextView?.layer.borderColor = UIColor.gray.cgColor
        bodyTextView?.layer.borderWidth = 0.5
        self.view.addSubview(bodyTextView!)
        bodyTextView?.snp.makeConstraints({ (make) in
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.top.equalTo(line.snp_bottom).offset(10)
            make.bottom.equalTo(-30)
        })
        
        if !isNew {
            titleTextField?.text = noteModel?.title
            bodyTextView?.text = noteModel?.body
        }
    }
}
