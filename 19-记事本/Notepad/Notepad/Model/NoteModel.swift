//
//  NoteModel.swift
//  Notepad
//
//  Created by 左忠飞 on 2021/1/20.
//

import UIKit

class NoteModel: NSObject {
    //记录时间
    var time:String?
    //标题
    var title:String?
    //内容
    var body:String?
    //所在分组
    var group:String?
    //主键
    var noteId:Int?
    
    //将数据模型属性直接转换为字典
    func toDictionary() -> Dictionary<String,Any> {
        var dic:[String:Any] = ["time":time!,
                                "title":title!,
                                "body":body!,
                                "ownGroup":group!]
        if let id = noteId {
            dic["noteId"] = id
        }
        return dic
    }
}
