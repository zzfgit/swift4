import UIKit

var str = "Hello, playground"

//为自己定义的数据类型提供下标访问功能,就像访问数组一样

class MyArray {
    var array:Array<Any>
    init(param:Int...) {
        array = param
    }
    
    //subscript是swift中用于定义下标功能的方法
    subscript(index:Int) -> Int{
        set{
            array[index] = newValue
        }
        get{
            return array[index] as! Int
        }
    }
}

var myarray = MyArray(param: 1,2,3,4,5)
myarray[2]
myarray[3] = 6
