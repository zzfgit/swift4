import UIKit

var str = "Hello, playground"

//编写一个函数，其功能是输入一个学生分数，打印输出分数所在的成绩等级，60分以下为不及格，60~75分之间为及格，76~90分之间为良好，90分以上为优秀。需要注意，分数的取值范围为0~100之间，输入范围之外的分数将抛出异常，需要打印分数无效的提示。

enum RateError:Error {
    case scoreIllegalError
}

func rateScore(mark:Int)throws{
    if mark < 0 {
        throw RateError.scoreIllegalError
    }else if mark < 60 {
        print("不及格")
    }else if mark < 80 {
        print("良")
    }else if mark < 90 {
        print("优")
    }else if mark < 101 {
        print("十分优秀")
    }else {
        throw RateError.scoreIllegalError
    }
}

do {
    try rateScore(mark: 99)
}catch RateError.scoreIllegalError {
    print("输入成绩格式不对")
}catch {
    print("2121")
}
