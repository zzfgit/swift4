import UIKit

//subscript方法可以为Swift中的类添加下标访问的支持。在Swift 4中，subscript方法更加强大，其不只可以支持泛型，而且可以支持where子句进行协议中关联类型的约束
//下标协议
protocol Sub {
    associatedtype T
    func getIndex() -> T
}

//实现下标协议的一种下标类
class Index:Sub {
    
    typealias T = Int

    var index:Int
    
    init(index:Int) {
        self.index = index
    }
    
    func getIndex() -> T {
        return self.index
    }
    
}

class MyArray {
    var array = Array<Int>()
    func push(item:Int){
        self.array.append(item)
    }
    
    //泛型,并进行约束
    subscript<T:Sub>(i:T) -> Int where T.T == Int {
        return self.array[i.getIndex()]
    }
}

var a = MyArray()
a.push(item: 1)
print(a[Index(index: 0)])
