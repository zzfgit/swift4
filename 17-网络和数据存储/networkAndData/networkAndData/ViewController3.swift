//
//  ViewController3.swift
//  networkAndData
//
//  Created by 左忠飞 on 2021/1/14.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController3: ViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //获取文件路径
//        let path = Bundle.main.path(forResource: "newPlist", ofType: "plist")
//        //将文件内容转成字典
//        let dict = NSDictionary(contentsOfFile: path!)
//        //数据输出
//        print(dict ?? "数据为nil")
        
        //存储plist文件
        let path2 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        //拼接上文件名
        let fileName = path2! + "/MyPlist.plist"
        let dic:NSDictionary = ["name":"王麻子","age":"12"]
        //进行写文件
//        dic.write(toFile: fileName, atomically: true)
        dic.write(toFile: fileName, atomically: true)
        //将存储的plist文件读取处理啊
        let dicRes = NSDictionary(contentsOfFile: fileName)
        print(dicRes ?? "没有找到存储的plist文件")
    }
    

    

}
