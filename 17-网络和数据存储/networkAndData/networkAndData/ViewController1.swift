//
//  ViewController1.swift
//  networkAndData
//
//  Created by 左忠飞 on 2021/1/14.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController1: ViewController {

    let urlString = "http://58.30.9.142:13029/rmcp/cloud/area/getAreaList";
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //创建请求配置
        let config = URLSessionConfiguration.default
        //创建请求URL
        let url = URL(string: urlString)
        //创建请求实例
        var request = URLRequest(url: url!)
        //如果不设置请求方法,默认是GET请求
        request.httpMethod = "POST"
        
        //创建请求session
        let session = URLSession(configuration: config)
        //创建请求任务
        let task = session.dataTask(with: request) { (data, response, error) in
            let dict = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
            print(dict ?? "未解析到数据")
        }
        task.resume()
    }
    

  

}
