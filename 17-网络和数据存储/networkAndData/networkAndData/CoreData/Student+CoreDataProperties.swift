//
//  Student+CoreDataProperties.swift
//  networkAndData
//
//  Created by 左忠飞 on 2021/1/16.
//  Copyright © 2021 左忠飞. All rights reserved.
//
//

import Foundation
import CoreData


extension Student {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Student> {
        return NSFetchRequest<Student>(entityName: "Student")
    }

    @NSManaged public var age: Int16
    @NSManaged public var name: String?

}
