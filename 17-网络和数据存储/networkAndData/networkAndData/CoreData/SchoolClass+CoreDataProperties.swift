//
//  SchoolClass+CoreDataProperties.swift
//  networkAndData
//
//  Created by 左忠飞 on 2021/1/16.
//  Copyright © 2021 左忠飞. All rights reserved.
//
//

import Foundation
import CoreData


extension SchoolClass {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SchoolClass> {
        return NSFetchRequest<SchoolClass>(entityName: "SchoolClass")
    }

    @NSManaged public var studentCount: Int16
    @NSManaged public var name: String?
    @NSManaged public var moniter: Student?

}
