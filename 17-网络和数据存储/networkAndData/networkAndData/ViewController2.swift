//
//  ViewController2.swift
//  networkAndData
//
//  Created by 左忠飞 on 2021/1/14.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController2: ViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //获取应用程序默认的userdefaults实例
        let userDefaults = UserDefaults.standard
        //进行URL数据的存储
        userDefaults.set(URL(string: "www.baidu.com"), forKey: "urlKey")
        //进行字符串数据的存储
        userDefaults.set("stringValue", forKey: "stringKey")
        //进行Bool值数据的储存
        userDefaults.set(true, forKey: "boolKey")
        //进行Double类型数据的存储
        userDefaults.set(Double(0.1), forKey: "doubleKey")
        //进行float类型数据的存储
        userDefaults.set(Float(1.5), forKey: "floatKey")
        //进行Int类型数据的储存
        userDefaults.set(5, forKey: "intKey")
        //进行字典数据的存储
        userDefaults.set(["1":"一"], forKey: "dictKey")
        //进行数组数据的存储
        userDefaults.set([1,2,3,4], forKey: "arrKey")
        //进行Data数据的存储
        userDefaults.set(Date(), forKey: "dataKey")
        //将操作进行同步
        userDefaults.synchronize()
        
        //数据的存储操作与写入磁盘之间有一定的延时，调用synchronize()方法可以将存储的数据立即同步到磁盘。
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let userDefaults = UserDefaults.standard
        
        print(userDefaults.string(forKey: "stringKey") ?? "没有数据")
        
    }
    

}
