//
//  TableViewController.swift
//  networkAndData
//
//  Created by 左忠飞 on 2021/1/14.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    override func viewDidLoad() {
         super.viewDidLoad()

         self.tableView.register(NSClassFromString("UITableViewCell"), forCellReuseIdentifier: "UITableViewCell")
     }

     // MARK: - Table view data source
     override func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }

     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 10
     }

    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)

         cell.textLabel?.text = "文字";

         switch indexPath.row {
         case 0:
             cell.textLabel?.text = "网络请求";
         case 1:
             cell.textLabel?.text = "UserDefaults";
         case 2:
             cell.textLabel?.text = "plist";
         case 3:
             cell.textLabel?.text = "解档归档";
         case 4:
             cell.textLabel?.text = "CoreData";
         case 5:
             cell.textLabel?.text = "基础动画和帧动画";
         case 6:
             cell.textLabel?.text = "例子效果";
         case 7:
             cell.textLabel?.text = "snapkit";
         case 8:
             cell.textLabel?.text = "使用WebView播放gif图片";
         default:
             print("没有默认项")
         }
         
         
         return cell
     }
     
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
         
         switch indexPath.row {
         case 0:
             self.navigationController?.pushViewController(ViewController1(), animated: true)
         case 1:
             self.navigationController?.pushViewController(ViewController2(), animated: true)
         case 2:
             self.navigationController?.pushViewController(ViewController3(), animated: true)
         case 3:
             self.navigationController?.pushViewController(ViewController4(), animated: true)
         case 4:
             self.navigationController?.pushViewController(ViewController5(), animated: true)
//         case 5:
//             self.navigationController?.pushViewController(ViewController6(), animated: true)
//         case 6:
//             self.navigationController?.pushViewController(ViewController7(), animated: true)
//         case 7:
//             self.navigationController?.pushViewController(ViewController8(), animated: true)
//         case 8:
//             self.navigationController?.pushViewController(ViewController4(), animated: true)
         default:
             print("没有默认项")
         }
     }

}
