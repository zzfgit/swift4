//
//  ViewController7.swift
//  Animation
//
//  Created by 左忠飞 on 2021/1/14.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController7: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "粒子效果"
        
        //例子发射引擎
        let fireEmitter = CAEmitterLayer()
        //设置发射引擎的位置和尺寸
        //将发射引擎放在屏幕底部中间
        fireEmitter.emitterPosition = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2)
        fireEmitter.emitterSize = CGSize(width: self.view.bounds.size.width, height: 20)
        //粒子渲染模式为混合渲染
        fireEmitter.renderMode = .additive
        
        //配置粒子单元
        let cell = CAEmitterCell()
        cell.birthRate = 1000
        cell.lifetime = 4.0
        cell.lifetimeRange = 1.5
        cell.color = UIColor.orange.cgColor
        let contnet = UIView(frame: CGRect(x: 0, y: 0, width: 3, height: 3))
        contnet.backgroundColor = .red
        cell.contents = contnet
        cell.velocity = 200
        cell.velocity = 100
        cell.emissionLongitude = CGFloat(Double.pi + Double.pi*2)
        cell.emissionRange = CGFloat(Double.pi*2)
        
        //配置粒子发射引擎的粒子单元
        fireEmitter.emitterCells = [cell]
        self.view.layer.addSublayer(fireEmitter)
    }
    


}
