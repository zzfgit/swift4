//
//  ViewController4.swift
//  Animation
//
//  Created by 左忠飞 on 2021/1/13.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit
import WebKit

class ViewController4: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        let webView = WKWebView(frame: CGRect(x: 0,
                                              y: 100,
                                              width: UIScreen.main.bounds.width,
                                              height: UIScreen.main.bounds.width))
        self.view.addSubview(webView)
        self.playGifOnWebView(name: "animation", webView: webView)
    }
    

    func playGifOnWebView(name:String,webView:WKWebView) {
        //创建素材路径
        let path = Bundle.main.path(forResource: name, ofType: "gif")
        //通过路径创建素材URL
        let url = URL(fileURLWithPath: path!)
        //将git素材读取成data数据
        let imageData = try! Data(contentsOf: url)
        //设置webView不许滚动
        webView.scrollView.bounces = false
        //设置webView背景透明
        webView.backgroundColor = .clear
        //设置webView自适应缩放
        
        //加载gif数据
        webView.load(imageData, mimeType: "image/gif", characterEncodingName: "", baseURL: URL(string: Bundle.main.bundlePath)!)
    }

}
