//
//  ViewController2.swift
//  Animation
//
//  Created by 左忠飞 on 2021/1/13.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    var animationView:UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        animationView = UIView(frame: CGRect(x: 20, y: 100, width: 280, height: 300))
        animationView?.backgroundColor = .red
        self.view.addSubview(animationView!)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let animationView2 = UIView(frame: CGRect(x: 20, y: 100, width: 280, height: 300))
        animationView2.backgroundColor = .blue
        
        //这个方法的实质是将原视图控件从其父视图上移除，而将新的视图控件添加到原控件的父视图上，其动画效果实际上作用在父视图上，这个方法可以直接切换整个视图控件，对于变化较大或者是完全无关联的两种视图的内容切换，这个方法十分受用。
        UIView.transition(from:animationView!, to: animationView2, duration: 2, options: .transitionCurlUp, completion: nil)
    }

}
