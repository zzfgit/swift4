//
//  ViewController6.swift
//  Animation
//
//  Created by 左忠飞 on 2021/1/14.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController6: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        
        let label = UILabel(frame: CGRect(x: 100, y: 100, width: 200, height: 50))
        label.textAlignment = .center
        label.backgroundColor = .orange
        label.text = "测试文本";
        self.view.addSubview(label)
        
        //基础动画
        //创建动画实例 keyPath为要进行属性动画的属性路径
        let basicAni = CABasicAnimation(keyPath: "transform.rotation.y")
        //从0度开始旋转
        basicAni.fromValue = NSNumber(value: 0)
        //旋转到180度
        basicAni.toValue = NSNumber(value: Double.pi * 2)
        //设置动画播放的时长
        basicAni.duration = 5
        //设置动画重复次数
        basicAni.repeatCount = 10
        //将动画所用与当前界面的layer上
        label.layer.add(basicAni, forKey: nil)
        
        
        let label2 = UILabel(frame: CGRect(x: 100, y: 200, width: 200, height: 50))
        label2.textAlignment = .center
        label2.backgroundColor = .orange
        label2.text = "测试文本2";
        self.view.addSubview(label2)
        //关键帧动画
        let keyframeAni = CAKeyframeAnimation(keyPath: "transform.rotation.y")
        keyframeAni.values = [NSNumber(value: 0),NSNumber(value: Double.pi / 4),NSNumber(value: 0),NSNumber(value: Double.pi)];
        keyframeAni.duration = 5
        keyframeAni.repeatCount = 10
        label2.layer.add(keyframeAni, forKey: "")
        
        
        let label3 = UILabel(frame: CGRect(x: 100, y: 300, width: 200, height: 50))
        label3.textAlignment = .center
        label3.backgroundColor = .orange
        label3.text = "测试文本3";
        self.view.addSubview(label3)
        
        //阻尼动画
        let springAni = CASpringAnimation(keyPath: "position.y")
        //模拟质量,必须大于0,默认为1,会影响惯性
        springAni.mass = 2
        //模拟弹簧劲度西数,必须大于0,值越大回弹越快
        springAni.stiffness = 50
        //设置阻尼系数,必须大于0,值越大回弹幅度越小
        springAni.damping = 0.2
        
        springAni.toValue = 400
        springAni.duration = 2
        springAni.repeatCount = 10
        label3.layer.add(springAni, forKey: "")
        
        
        let label4 = UILabel(frame: CGRect(x: 100, y: 500, width: 200, height: 50))
        label4.textAlignment = .center
        label4.backgroundColor = .orange
        label4.text = "测试文本4";
        //转场动画
        let transAni = CATransition()
        //设置转场动画类型
        transAni.type = .push
        //设置转场动画方向
        transAni.subtype = .fromLeft
        
        label4.layer.add(transAni, forKey: "")
        self.view.addSubview(label4)

        
        let label5 = UILabel(frame: CGRect(x: 100, y: 500, width: 200, height: 50))
        label5.textAlignment = .center
        label5.backgroundColor = .orange
        label5.text = "测试文本5";
        //组合动画
        let basicAni1 = CABasicAnimation(keyPath: "backgroundColor")
        basicAni1.toValue = UIColor.green.cgColor
        //创见形变动画
        let basicAni2 = CABasicAnimation(keyPath: "transform.scale.x")
        basicAni2.toValue = NSNumber(value: 2)
        //进行动画组合
        let groupAni = CAAnimationGroup()
        groupAni.animations = [basicAni1,basicAni2];
        groupAni.duration = 3
        label5.layer.add(groupAni, forKey: "")
        self.view.addSubview(label5)
    }
    
}
