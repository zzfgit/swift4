//
//  ViewController3.swift
//  Animation
//
//  Created by 左忠飞 on 2021/1/13.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit
import ImageIO

class ViewController3: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        let imageView = UIImageView(frame: CGRect(x: 100, y: 100, width: 200, height: 200))
        imageView.contentMode = .scaleAspectFit
        self.view.addSubview(imageView)
        self.playGifOnImageView(name: "animation", imageView: imageView)
    }
    

    func playGifOnImageView(name:String,imageView:UIImageView) {
        //1创建素材路径
        let path = Bundle.main.path(forResource: name, ofType: "gif")
        //创建素材URL
        let url = URL(fileURLWithPath: path!)
        //创建实例
        let source = CGImageSourceCreateWithURL(url as CFURL, nil)
        //获取素材中图片的张数
        let count = CGImageSourceGetCount(source!)
        //创建数组所有图片
        var imageArray = Array<UIImage>()
        //创建数组保存图片的宽度
        var imagesWidth = Array<Int>()
        //创建数组保存图片的高度
        var imagesHeight = Array<Int>()
        //用于存放Gif的播放时长
        var time:Int = Int()
        
        //遍历素材
        for index in 0..<count {
            //从素材实例中获取图片
            let image = CGImageSourceCreateImageAtIndex(source!, index, nil)
            //将图片加入数组
            imageArray.append(UIImage(cgImage: image!))
            //获取图片信息数组
            let info = CGImageSourceCopyPropertiesAtIndex(source!, index, nil) as! Dictionary<String,AnyObject>
            //获取宽高
            let width = Int(truncating: info[kCGImagePropertyPixelWidth as String]! as! NSNumber)
            let height = Int(truncating: info[kCGImagePropertyPixelHeight as String]! as! NSNumber)
            
            imagesWidth.append(width)
            imagesHeight.append(height)
            
            //获取时间信息
            let timeDic = info[kCGImagePropertyGIFDictionary as String] as! Dictionary<String,AnyObject>
            //进行时间累加
            time += Int(truncating: timeDic[kCGImagePropertyGIFDelayTime as String] as! NSNumber)
        }
        
        //重设ImageView尺寸
        //大部分Gif文件中的图片尺寸相同,这里随便取一个即可
//        imageView.frame = CGRect(x: 0, y: 100, width: imagesWidth[0], height: imagesHeight[0])
        imageView.frame = CGRect(x: 0, y: 100, width: Int(UIScreen.main.bounds.size.width), height: Int(UIScreen.main.bounds.size.width))

        //进行动画播放
        imageView.animationImages = imageArray
        imageView.animationDuration = TimeInterval(time)
        imageView.animationRepeatCount = 0
        imageView.startAnimating()
    }

}
