//
//  ViewController5.swift
//  Animation
//
//  Created by 左忠飞 on 2021/1/13.
//  Copyright © 2021 左忠飞. All rights reserved.
//

import UIKit

class ViewController5: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        //渐变图层
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
        gradientLayer.position = CGPoint(x: 100, y: 200)
        
        gradientLayer.colors = [UIColor.red.cgColor,
                                UIColor.green.cgColor,
                                UIColor.blue.cgColor];
        
        gradientLayer.locations = [NSNumber(value: 0.2),NSNumber(value: 0.5),NSNumber(value: 0.7)];
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        self.view.layer.addSublayer(gradientLayer)
        
        //创建复制图层
        let replicatorLayer = CAReplicatorLayer()
        replicatorLayer.position = CGPoint.zero
        //创建内容图层
        let subLayer = CALayer()
        subLayer.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
        subLayer.position = CGPoint(x: 50, y: 400)
        subLayer.backgroundColor = UIColor.red.cgColor
        replicatorLayer.addSublayer(subLayer)
        
        //设置每次赋值将副本沿x轴平邑30
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(30, 10, 0)
        //设置复制副本的个数
        replicatorLayer.instanceCount = 10
        self.view.layer .addSublayer(replicatorLayer)
        
        //图形layer层,可以自定义图形
        let shapeLayer = CAShapeLayer()
        shapeLayer.position = CGPoint.zero
        //创建图形路径
        let path = CGMutablePath()
        //设置路径起点
        path.move(to: CGPoint(x: 200, y: 100))
        //进行划线
        path.addLine(to: CGPoint(x: 400, y: 100))
        path.addLine(to: CGPoint(x: 300, y: 200))
        path.addLine(to: CGPoint(x: 200, y: 100))
        //设置图层路径
        shapeLayer.path = path
        //设置图形边缘线条起点
        shapeLayer.strokeStart = 0
        //设置图形边缘线条终点
        shapeLayer.strokeEnd = 1
        
        //设置填充规则
        shapeLayer.fillRule = .evenOdd
        //设置填充颜色
        shapeLayer.fillColor = UIColor.red.cgColor
        //设置边缘线条颜色
        shapeLayer.strokeColor = UIColor.orange.cgColor
        //设置边缘线条宽度
        shapeLayer.lineWidth = 1
        self.view.layer.addSublayer(shapeLayer)
    }
}
