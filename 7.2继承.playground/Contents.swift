import UIKit

var str = "Hello, playground"

//设计一个交通工具类
class Transportation {
    //油量,默认提供10
    var petrol:Int = 10
    //行驶的方法
    func drive() {
        if petrol == 0 {
            self.addPetrol()
        }
    }
    //加油的方法
    func addPetrol() {
        petrol += 10
    }
}

//创建子类汽车,轮船,飞机
class Car:Transportation {
    //轮胎数量属性
    var tyre:Int
    
    //重写父类方法
    override func drive() {
        super .drive()
        print("在路上行驶了10公里")
        self.petrol -= 1
    }
    
    init(tyreCount:Int) {
        tyre = tyreCount
    }
}

class Boat:Transportation {
    //轮船的船舱层数
    var floor:Int
    //重写父类的方法
    override func drive() {
        super .drive()
        print("在海上行驶了20公里")
        self.petrol -= 1
    }
    init(floorCount:Int) {
        floor = floorCount
    }
}

class Airplane:Transportation {
    //定义飞机的飞行高度属性
    var height:Int
    //重写父类方法
    override func drive() {
        super .drive()
        print("在天上飞了100公里")
        self.petrol -= 1
    }
    
    init(height:Int) {
        self.height = height
    }
}

//创建类的实例
var benchi = Car(tyreCount: 4)
var titantic = Boat(floorCount: 8)
var a380 = Airplane(height: 10000)

//调用实例方法
benchi.drive()
titantic.drive()
a380.drive()


//Swift语言中还提供了一个final关键字，final关键字用于修饰某些终极的属性、方法或者类。被final修饰的属性和方法不能够被子类覆写
//同样，如果不希望某个类被继承，也可以使用final关键字来修饰这个类，使其成为终极类
