import UIKit

var str = "Hello, playground"

//在构造方法中,必须为存储属性赋值,
//有两种情况不用在构造方法中赋值
//1:声明储存属性时设置默认值
//2:懒加载属性
//3:可选类型属性

class MyClass {
    var count:Int = 0 {
        willSet{
            print("count属性willset")
        }
    }
    var name:String {
        didSet{
            print("name属性didset")
        }
    }
    
    var insterest:String?
    
    init() {
        name = "张三"
    }
    
    init(name:String) {
        self.name = name
    }
}

//在对存储属性设置默认值或者在构造方法中对其构造时，并不会触发属性监听器，只有在构造完成后，再次对其赋值时才会触发。

var cls1 = MyClass()
print(cls1.count)
print(cls1.name)
print(cls1.insterest as Any)


//对于结构体，开发者可以不实现其构造方法，编译器会默认生成一个构造方法，将所有属性作为参数
//如果开发者为值类型结构（例如结构体）提供了一个自定义的构造方法，那么系统默认生成的构造方法将失效，Swift语言这样设计的初衷是为了安全性方面做考虑。防止开发者调用自定义的构造方法却误调用到系统生成的构造方法。并且，对于值类型，构造方法可以嵌套使用，
